import re
from itertools import islice
import operator
results = []
words = []
wordlist =[]
frequencies = []
indexArr = []
finalList = {}
with open('hw1_word_counts_06.txt', 'rb') as f:
    for line in f:
		results.append(line.strip().split(" "))
		words.append(line.strip().split(" ")[0])
		frequencies.append(line.strip().split(" ")[1]);
wordslist = words	
#print wordslist	
results = sorted(results, key=lambda e : int(e[1]))
#least frequent
print(results[:8])
#most frequent
print(results[-8:])
#sum of all frequencies
sumFreq = sum(int(i[-1]) for i in results)
#computing probabilities of each word
for i in results:
    i[-1] = float(i[-1])/sumFreq
#take input of state
state = raw_input('Enter the state: ')
#take incorrectly guessed
incorrect = raw_input('Enter incorrectly guessed: ')
#find words with pattern
r = re.compile(state)
words = filter(r.match, words)
#find alphabet occurrence 
allTheLetters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","Q","U","V","W","X","Y","Z"]
for letter in allTheLetters:
    finalList[letter] = 0
sum = 0
wordFinal = []
#remove words with incorrectly guessed letterss 
incorrectLength = len(incorrect)
flag = 0
if incorrectLength == 0:
	wordFinal = words
else:
	for word in words:
		flag = 0
		for c in incorrect:
			if c in word:
				flag = 1
				break
		if flag != 1:
			wordFinal.append(word)
#remove words with already occurred letters
wordsInState = ""
indexOfWordsInState = []
for idx, val in enumerate(state):
    if "." not in val:
		wordsInState = wordsInState + val
wordsFinalWithoutStateAlphabets = []
for word in wordFinal:
	flag = 0
	for idx, val in enumerate(state):
		if val == '.' and word[idx] in wordsInState:
			flag = 1
			break
	if flag != 1:
		wordsFinalWithoutStateAlphabets.append(word);
#compute probability 		
for letter in allTheLetters: 	 
    if letter not in incorrect and  letter not in state:
		myListForAlphabet = [s for s in wordsFinalWithoutStateAlphabets if letter in s]
		for word in myListForAlphabet:
		    myIndex = wordslist.index(word)
		    finalList[letter] = int(finalList[letter]) + int(frequencies[int(myIndex)])
		    if myIndex not in indexArr:
			    sum = sum + int(frequencies[int(myIndex)])
			    indexArr.append(myIndex)
#sort with max probability in the beginning
for key in finalList:
    finalList[key] = float(finalList[key])/float(sum)
nextBestGuess = max(finalList.iteritems(), key=operator.itemgetter(1))[0]
print "Your next best guess is: " + str(nextBestGuess)
print "Probability: " + str(finalList[nextBestGuess])

